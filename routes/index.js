var express = require('express');
var router = express.Router();
var SensorController = require('../controllers/sensor');
var EmailController = require('../controllers/emailController');
var authentificationController = require('../controllers/authentificationController');
var Sensor = require('../model/sensor');
var session = require('express-session');
var User = require('../model/user');
var passwordHash = require('password-hash');


//GET PAGES TO VIEW
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Tyson'});
});

router.get('/dashboard', authentificationController.requiredAuthentication, function (req, res, next) {
    Sensor.find(function (e, senslist) {
        res.render('dashboard', {"sensorlist": JSON.stringify(senslist)});
    });

});
router.get('/omc', authentificationController.requiredAuthentication, function (req, res, next) {
    Sensor.find(function (e, senslist) {
        res.render('omc', {"sensorlist": JSON.stringify(senslist)});
    });
});


//---------------USER AUTH---------------//
router.get('/login', function (req, res) {
    res.render('login');
});
router.post("/login", function (req, res) {
    authentificationController.login(req.body.username, req.body.password, function (err, user) {
        if (err) throw err;
        if (user) {
            req.session.regenerate(function () {
                req.session.user = user;
                req.session.success = 'Authenticated as ' + user.username + ' click to <a href="/logout">logout</a>. ' + ' You may now access <a href="/restricted">/restricted</a>.';
                res.redirect('/');
            });
        } else {
            req.session.error = 'Authentication failed, please check your ' + ' username and password.';
            res.redirect('/login');
        }
    })
});
router.get('/logout', authentificationController.logout);
router.get('/signup', function (req, res) {
    res.render('signup')
});
router.post("/signup", authentificationController.userExist, function (req, res) {
    var password = passwordHash.generate(req.body.password);
    var username = req.body.username;
    var email = req.body.email;
    var firstName = req.body.firstname;
    var lastName = req.body.lastname;
    var userLevel = req.body.userLevelSelect;

    var user = new User({
        username: username,
        email: email,
        firstName: firstName,
        lastName: lastName,
        password: password,
        accessLevel: userLevel
    }).save(function (err, newUser) {
        if (err) throw err;
        authentificationController.login(newUser.username, password, function (err, user) {
            if (user) {
                req.session.regenerate(function () {
                    req.session.user = user;
                    req.session.success = 'Authenticated as ' + user.username + ' click to <a href="/logout">logout</a>. ' + ' You may now access <a href="/restricted">/restricted</a>.';
                    res.redirect('/');
                });
            }
        });
    });
});

//CREATE
router.post('/sensors/create', SensorController.create);

//READ
router.get('/sensors/:id/read', SensorController.read);

//UPDATE
router.put('/sensors/:id/update', SensorController.update);

//DELETE
router.delete('/sensors/:id/delete', SensorController.delete);

//EMAIL
router.post('/sendEmail', EmailController.generate);

module.exports = router;

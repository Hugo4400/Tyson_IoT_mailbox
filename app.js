var express = require('express');

/*Middleware*/
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var index = require('./routes/index');
var users = require('./routes/users');
var expressSession = require('express-session');
var createServer = require('auto-sni');

var app = express();
createServer({
    email: "hugo4400@gmail.com", // Emailed when certificates expire.
    agreeTos: true,
    debug: true, // Add console messages and uses staging LetsEncrypt server. (Disable in production)
    domains: ["localhost:3000"], // List of accepted domain names. (You can use nested arrays to register bundles with LE).
    forceSSL: true, // Make this false to disable auto http->https redirects (default true).
    redirectCode: 301, // If forceSSL is true, decide if redirect should be 301 (permanent) or 302 (temporary). Defaults to 302
    ports: {
        http: 2999, // Optionally override the default http port.
        https: 3000 // // Optionally override the default https port.
    }
},app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var sess = {
    secret: 'encrypted',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
};
app.use(expressSession(sess));


app.use('/', index);
app.use('/users', users);


//Mongoose connection to DB
mongoose.connect('mongodb://localhost/sensors');

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

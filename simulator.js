/**
 * Created by hugo on 18/01/2017.
 */

var request = require('request');

//open and parse .ini file
var fs = require('fs');
var config = fs.readFileSync("./simulatorConfig.ini","UTF-8");
var configParse = JSON.parse(config);

//Assign parsed data to variables
var hostAddr = configParse.Topology.HostAddress,
    lightSensors = +configParse.Topology.TypesOfSensors.Light,
    laserSensors = +configParse.Topology.TypesOfSensors.Laser,
    partitions = +configParse.Topology.NumberOfPartitions,
    portNum = configParse.Topology.PortNumber,
    sensorTotal = lightSensors + laserSensors,
    path = '/sensors/create';


//60000 for one minute
var intervalTimer = 6000;
setInterval(function(){
    for (var i = 0; i < partitions; i++) {
        for(var j = 0; j < sensorTotal; j++){
            var options, dataVar;
            if(j < lightSensors){
                var brightness = Math.floor(Math.random() * 11);
                dataVar = {"sensorID":j,"partition": ""+i+"","type" : "Light", "value": brightness};
                options = {
                    url: hostAddr + portNum + path,
                    method: 'POST',
                    form: dataVar
                };
                 console.log(options);

                request(options,function (error,response,body){
                    console.log("server: " + body);
                });
            } else {
                var tripped = Math.floor(Math.random()*2);
                dataVar = {"sensorID":j,"partition": ""+i+"", "type": "Laser", "value": tripped};
                options = {
                    url: hostAddr + portNum + path,
                    method: 'POST',
                    form: dataVar
                };

                request(options,function (error,response,body){
                    if(!error && response.statusCode == 200){
                        console.log("Server: " + body);
                    }
                });
            }
        }
    }
},intervalTimer);

/**
 * Created by hugo on 13/02/2017.
 */
var session = require('express-session');
var User = require('../model/user');
var hashPassword = require('password-hash');

var authentificationController = {
    login: function (name, pass, fn) {
        if (module.parent) console.log('authenticating %s:%s', name, pass);
        User.findOne({
                username: name
            },
            function (err, user) {
                if (user) {
                    if (err)
                        return fn(new Error('cannot find user'));
                    if (hashPassword.verify(pass, user.password))
                        return fn(null, user);
                    else
                        fn(new Error('invalid password'));
                } else
                    return fn(new Error('cannot find user'));
            });
    },
    logout: function (req, res) {
        if (req.session.views != 0) {
            console.log("Session" + req.session.name + " disconnected");
            req.session.destroy();
            res.write("<h1>Disconnected</h1>");
        } else
            res.write("<h1>Session not found</h1>");
    },
    requiredAuthentication: function (req, res, next) {
        if (req.session.user)
            next();
        else {
            req.session.error = 'Access denied!';
            res.redirect('/login');
        }
    },
    userExist: function (req, res, next) {
        User.count({
            username: req.body.username
        }, function (err, count) {
            if (count === 0)
                next();
            else {
                req.session.error = "User Exist";
                res.redirect("/signup");
            }
        });
    }
};

module.exports = authentificationController;

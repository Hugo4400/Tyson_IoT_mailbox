/**
 * Created by hugo on 08/02/2017.
 */

var pdf = require('html-pdf');
var cheerio = require('cheerio');
var nodemailer = require('nodemailer');

var EmailController = {
    generate: function (req, res) {
        var $ = cheerio.load(req.body.html);
            pdf.create($.html()).toFile('./emailReport.pdf', function (err, res) {
                console.log("PDF");
        });
        var transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'hugo.moracchini.acs@gmail.com',
                pass: '123azerty'
            }
        });
        var mailOptions = {
            from: 'Web server <hugo.moracchini.acs@gmail.com>',
            to: 'hugo.moracchini.acs@gmail.com',
            subject: "Dashboard email report",
            text: "You have requested an email report of Tyson partition.",
            html: "<b>Latest report</b>",
            attachments: [
                {
                    finename: 'emailReport.pdf',
                    path: 'C:/Users/hugo/OneDrive/ULR/L3/Client-Server Architecture/TP2/emailReport.pdf',
                    contentType: 'application/pdf'
                }]
        };
        transporter.sendMail(mailOptions, function(error){
            if (error) {
                return console.log(error);
            }
            console.log('Message sent');
        });
    }
};

module.exports = EmailController;

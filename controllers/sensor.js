/**
 * Created by hugo on 25/01/2017.
 */

var Sensor = require('../model/sensor');
var SensorController = {
    create: function (req, res) {
        var sensor = new Sensor(req.body);
        sensor.save(function (err, sensor) {
            if (err) res.sendStatus(500);
            else res.sendStatus(200);
        });
    },
    read: function (req, res) {
        Sensor.findOne({"_id": req.params.id}, function (err, sensor) {
            if (err) res.sendStatus(500);
            else res.send(sensor);
        });
    },
    update: function (req, res) {
        console.log(req.body);
        Sensor.findOneAndUpdate({"_id": req.params.id}, req.body, function (err, sensor) {
            if (err) res.sendStatus(500);
            else res.sendStatus(200);
        });
    },
    delete: function (req, res) {
        Sensor.findOneAndRemove({"_id": req.params.id}, function (err, sensor) {
            if (err) res.sendStatus(500);
            else res.sendStatus(200);
        });
    }
};
module.exports = SensorController;

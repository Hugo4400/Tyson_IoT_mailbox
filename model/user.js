/**
 * Created by hugo on 13/02/2017.
 */
var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    username: {type:String, unique:true},
    email: String,
    firstName: String,
    lastName: String,
    password: String,
    accessLevel: Number
});

var User = mongoose.model('user', userSchema);
module.exports = User;
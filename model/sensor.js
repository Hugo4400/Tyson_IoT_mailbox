/**
 * Created by hugo on 25/01/2017.
 */

var mongoose = require('mongoose');
var sensorSchema = mongoose.Schema({
    sensorID: {type: Number, unique: true},
    partition: String,
    type: String,
    value: Number
}, {timestamps: true});
var Sensor = mongoose.model('sensor', sensorSchema);
module.exports = Sensor;